import 'package:dollars/screens/home_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (_) => HomeScreen(),
      },
      theme: ThemeData(
          brightness: Brightness.dark,
          accentColor: Colors.grey[900],
          buttonTheme: ButtonThemeData(
            textTheme: ButtonTextTheme.accent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(14.0),
              side: BorderSide(
                color: Colors.grey[900],
                width: 3,
              ),
            ),
          ),
          textTheme: const TextTheme(
            bodyText1: TextStyle(fontFamily: 'Microsoft Yahei'),
            bodyText2: TextStyle(fontFamily: 'Microsoft Yahei'),
          )),
    ));
