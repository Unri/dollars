import 'package:flutter/cupertino.dart';
import 'package:sms_services/sms_services.dart';

/// A Thread of SMS Message
class SmsThread implements Comparable<SmsThread> {
  final int _id;
  final String _contact;
  final String _address;
  final List<SmsMessage> _messages;

  /// Create an empty Thread
  SmsThread(this._id, this._contact, this._address, this._messages);

  /// Create a thread from a list of message
  static SmsThread fromMessageList({
    @required String contact,
    @required List<SmsMessage> messageList
  }) =>
    SmsThread(
      messageList.first.threadId,
      contact,
      messageList.first.address,
      messageList
    );

  ///The thread ID.
  int get id => _id;
  /// The address of the other party.
  String get address => _address;
  /// The name of the other party.
  String get contact => _contact;
  /// Get a list of the thread's messages
  List<SmsMessage> get messages => _messages;
  /// Get the last message of the thread
  SmsMessage get snippet => _messages.last;
  /// Get the count of messages
  int get count => _messages.length;

  /// Sort the thread messages from later to recent
  void sortMessages([int compare(SmsMessage a, SmsMessage b)]) {
    _messages.sort(compare
      ?? (a, b) => a.date.millisecondsSinceEpoch - b.date.millisecondsSinceEpoch);
  }
  /// Add a message at the end of the message list
  void addMessage(SmsMessage message) {
    _messages.add(message);
  }
  /// Insert a message at the specified index of the list
  void insertMessage(int index, SmsMessage message) {
    _messages.insert(index, message);
  }

  @override
  int compareTo(SmsThread other) =>
    other.snippet.date.millisecondsSinceEpoch - snippet.date.millisecondsSinceEpoch;
}
