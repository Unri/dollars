import 'package:dollars/model/sms_thread.dart';
import 'package:flutter/services.dart';
import 'package:sms_services/sms_services.dart';

/// Get the list of SMS threads
Future<List<SmsThread>> getThreads() async {
  final List<SmsMessage> smsList = await SmsServices.getSms();
  final List<Contact> contacts = await SmsServices.getContacts();
  if (smsList == null) {
    await SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }
  final Map<int, List<SmsMessage>> threadMap = {};
  for (final msg in smsList) {
    if (threadMap[msg.threadId] == null) {
      threadMap[msg.threadId] = [];
    }
    threadMap[msg.threadId].add(msg);
  }
  final List<SmsThread> threadList= [];
  for (final list in threadMap.values) {
    threadList.add(SmsThread.fromMessageList(
      messageList: list,
      contact: contacts.firstWhere(
        (el) => el.phoneNumber == list.first.address,
        orElse: () => null)?.name
        ?? list.first.address
    ));
  }
  threadList.sort((a, b) =>
    b.snippet.date.millisecondsSinceEpoch - a.snippet.date.millisecondsSinceEpoch);
  return threadList;
}