import 'package:dollars/components/thread/thread_snippet.dart';
import 'package:dollars/model/sms_thread.dart';
import 'package:dollars/screens/chat_screen.dart';
import 'package:dollars/services/sms_service.dart';
import 'package:flutter/material.dart';

/// The home screen
class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static const String _appbarTitle = 'Messages';
  List<SmsThread> _threadList = [];
  bool _isLoaded = false;

  void setup() async {
      final List<SmsThread> list = await getThreads();
      setState(() {
        _threadList = list;
        _isLoaded = true;
      });
  }

  void _openChatScreen(BuildContext context, SmsThread thread) async {
    await Navigator.of(context).push(
      PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => ChatScreen(thread),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          final tween = Tween(
            begin: const Offset(1, 0),
            end: const Offset(0, 0),
          ).chain(CurveTween(curve: Curves.ease));
          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        },
      )
    );
    // Update sort and update the thread list
    _threadList.sort();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    setup();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(_appbarTitle),
        centerTitle: true,
      ),
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (OverscrollIndicatorNotification overScroll) {
          overScroll.disallowGlow();
          return false;
        },
        child: SafeArea(
          child: Scrollbar(
            child: AnimatedOpacity(
              opacity: _isLoaded ? 1.0 : 0.0,
              duration: const Duration(seconds: 1),
              child: ListView.builder(
                itemCount: _threadList.length,
                itemBuilder: (_, i) => ThreadSnippet(
                  thread: _threadList[i],
                  onTap: () => _openChatScreen(context, _threadList[i]))
              ),
            ),
          ),
        ),
      )
    );
  }
}
