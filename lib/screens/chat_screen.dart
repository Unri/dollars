
import 'package:dollars/components/chat/message_bubble.dart';
import 'package:dollars/model/sms_thread.dart';
import 'package:dollars/components/chat/chat_input_pane.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sms_services/sms_services.dart';

/// The chat screen
class ChatScreen extends StatelessWidget {
  /// The chat screen SMS thread
  final SmsThread thread;
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  final _textController = TextEditingController(text: '');

  /// Create a SMS chat screen
  ChatScreen(SmsThread this.thread);

  void _sendMessage() {
    if (_textController.text == '') {
      return;
    }
    final sendDate = DateTime.now().millisecondsSinceEpoch;
    thread.addMessage(SmsMessage.fromMap({
      'address': thread.address,
      'body': _textController.text,
      'date': sendDate,
      'date_sent': sendDate,
      'status':0,
      'type': SmsMessageType.SENT.index
    }));
    _listKey.currentState.insertItem(0);
    SmsServices.sendSms(
      destinationAddress: thread.address,
      message: _textController.text
    );
    _textController.clear();
  }

  Widget _messageBuilder(BuildContext context, SmsMessage msg, Animation<double> animation) =>
    Padding(
      padding: const EdgeInsets.all(10),
        child: MessageBubble(
          animation: animation,
          message: msg.body,
          isSent: msg.type == SmsMessageType.SENT,
          gradient: const [
            Color(0xFF81767E),
            Color(0xFF796E76),
            Color(0xFFA09599),
            Color(0xFFEEECEC),
          ]),
    );

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus.unfocus();
      },
      child: Scaffold(
          appBar: AppBar(
            leading: BackButton(
              color: Colors.white,
              onPressed: () => Navigator.pop(context),
            ),
            title: Text(thread.contact),
          ),
          body: SafeArea(
            child: Column(children: <Widget>[
              Expanded(
                child: AnimatedList(
                  key: _listKey,
                  reverse: true,
                  shrinkWrap: true,
                  initialItemCount: thread.count,
                  itemBuilder: (_, i, animation) =>
                    _messageBuilder(_, thread.messages[thread.count - i - 1], animation)
                  )

              ),
              ChatInputPane(
                textEditingController: _textController,
                onSubmitMessage: _sendMessage
              ),
            ]),
          )),
    );
  }
}
