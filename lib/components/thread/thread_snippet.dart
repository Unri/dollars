
import 'package:dollars/model/sms_thread.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

/// A thread's snippet
class ThreadSnippet extends StatelessWidget {
  /// The most recent message of the thread
  final SmsThread thread;
  /// On tap event handler
  final Function onTap;

  /// Construct a Thread snippet
  const ThreadSnippet({
    this.thread,
    this.onTap
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
        leading: FadeInImage(
          placeholder: MemoryImage(kTransparentImage),
          image: const AssetImage('assets/chat_icon/setton.jpg'),
          fadeInDuration: const Duration(milliseconds: 100),
          height: 40,
          width: 40,
        ),
        title: Text(thread.contact),
        subtitle: Text(
          thread.snippet.body,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
        ),
        onTap: onTap
      ),
    );
  }
}