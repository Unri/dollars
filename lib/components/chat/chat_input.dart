
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// The chat page input text
class ChatInput extends StatelessWidget {

  /// Create a chat input
  ChatInput({this.controller});

  /// Text input controller
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      cursorColor: Colors.grey[900],
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
        filled: true,
        fillColor: Colors.white,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(14.0),
          borderSide: BorderSide(
            color: Colors.grey[900],
              width: 3,
          )
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(14.0),
          borderSide: BorderSide(
            color: Colors.grey[900],
              width: 3,
          )
        ),
        floatingLabelBehavior: FloatingLabelBehavior.never,
      ),
      maxLines: null,
      style: const TextStyle(color: Colors.black),
    );
  }
}