import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

/// SMS Chat message component
class MessageBubble extends StatelessWidget {
  static const _borderRadius = Radius.circular(20);

  /// The SMS message
  final String message;

  /// The Message grandiant background
  final List<Color> gradient;

  /// Flag indicating if the message is SENT type
  final bool isSent;

  /// Initial animation value
  final Animation<double> animation;

  /// Create a SMS Chat message
  MessageBubble({@required this.gradient, this.message = '', this.animation, this.isSent});

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    return ScaleTransition(
      scale: CurvedAnimation(curve: BouncingCurve(), parent: animation),
      alignment: isSent ? Alignment.centerRight : Alignment.centerLeft,
      child: Align(
        alignment: isSent ? Alignment.centerRight : Alignment.centerLeft,
        child: Container(
          constraints: BoxConstraints(maxWidth: deviceWidth * 0.7),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              stops: const [0.2, 0.5, 0.65, 1],
              colors: gradient,
            ),
            border: Border.all(
              color: Colors.white,
              width: 5,
            ),
            borderRadius: BorderRadius.only(
              topRight: !isSent ? _borderRadius : Radius.zero,
              topLeft: isSent ? _borderRadius : Radius.zero,
              bottomLeft: _borderRadius,
              bottomRight: _borderRadius,
            ),
          ),
          child: Text(message),
        ),
      ),
    );
  }
}

/// Single Bouncing curve
class BouncingCurve extends Curve {
  @override
  double transformInternal(double t) =>
    sin(t * 2) * 1.1;
}