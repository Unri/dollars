
import 'package:dollars/components/chat/chat_input.dart';
import 'package:dollars/components/chat/post_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Chat messaging input pane
class ChatInputPane extends StatelessWidget {
  static const double _formFieldPadding = 6.0;
  static const String _buttonText = 'POST!';
  /// Input text editing controller
  final TextEditingController textEditingController;
  /// On submit message handler
  final Function onSubmitMessage;

  /// Create a Chat input panel
  ChatInputPane({
    this.textEditingController,
    this.onSubmitMessage
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: const [0.3, 0.7, 1],
          colors: <Color>[
            Colors.grey[300],
            Colors.grey[100],
            Colors.grey[300],
          ],
        )
      ),
      padding: const EdgeInsets.symmetric(vertical: _formFieldPadding),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              margin: const EdgeInsets.fromLTRB(_formFieldPadding, 0, _formFieldPadding / 2, 0),
              child: ChatInput(controller: textEditingController,),
            ),
          ),
          Container(
            width: 70,
            margin: const EdgeInsets.fromLTRB(_formFieldPadding / 2, 0, _formFieldPadding, 0),
            child: PostButton(
              buttonText: _buttonText,
              onPressed: onSubmitMessage
            ),
          ),
      ]),
    );
  }
}
