import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// The chat page Post button
class PostButton extends StatelessWidget {
  static const Color _buttonColor = Color(0xFFD6D6D6);

  /// Create a Post button
  const PostButton({
    @required this.buttonText,
    this.onPressed,
  });

  /// The Post button text
  final String buttonText;
  /// The callback that is called when the button is tapped or otherwise activated
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      padding: const EdgeInsets.symmetric(vertical: 13.5),
      onPressed: onPressed,
      color: _buttonColor,
      child: Text(buttonText,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 18,
        )),
    );
  }
}
